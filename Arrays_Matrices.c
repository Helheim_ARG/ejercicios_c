#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
// Necesaria para incluir el strcpy
#include <string.h>

// Declaración de las Estructuras
// Persona
struct persona {
    char nombre[12];
    char apellido[12];
    int edad;
};

int main(){
    // Definimos un Array de Elementos del tipo Estructura Persona
    struct persona alumnos[2];
    // Asignamos los valores para el Elemento 0
    strcpy(alumnos[0].nombre,"Pedro");
    alumnos[0].edad=13;
    // Asignamos los valores para el Elemento 1
    strcpy(alumnos[1].nombre,"Cristian");
    alumnos[1].edad=14;
    // Recorremos y mostramos
    printf("\n _Arrays_ \n\n");
    for(int i=0;i<2;i++){
       printf("Nombre -> %s , Edad -> %d \n",alumnos[i].nombre,alumnos[i].edad);
    }

    // Definimos un Array de Elementos del tipo Estructura Persona
    struct persona alumnos2[1][2];
    // Asignamos los valores para el Elemento 0
    strcpy(alumnos2[0][0].nombre,"Pedro");
    alumnos2[0][0].edad=13;
    // Asignamos los valores para el Elemento 1
    strcpy(alumnos2[0][1].nombre,"Cristian");
    alumnos2[0][1].edad=14;
    // Recorremos y mostramos
    printf("\n _Matriz_ \n\n");
    for(int i=0;i<1;i++){
        for(int j=0;j<2;j++){
            printf("Nombre -> %s , Edad -> %d \n",alumnos2[i][j].nombre,alumnos2[i][j].edad);
        }
    }
}






