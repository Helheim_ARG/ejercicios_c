#include <stdio.h>
#include <stdbool.h>
#include <conio.h>
float fn_suma (float a, float b);
float fn_resta (float a, float b);
float fn_multiplicacion (float a, float b);
float fn_division (float a, float b);


int main ()
{

    float a, b, suma, resta, multiplicacion, division;
    bool salir=false;
    int opcion;

    do
    {
        printf("ingrese una opcion \n");
        printf("1. sumar \n");
        printf("2. restar \n");
        printf("3. multiplicar \n");
        printf("4. dividir \n");
        printf("5. salir \n");
        scanf("%d", &opcion);
        switch (opcion)
        {
        case 1:
            printf("ingrese dos valores numericos separados por coma, para realizar una operacion matematica \n");
            scanf("%f,%f", &a,&b);
            suma=fn_suma(a,b);
            printf("la suma da: %0.2f \n", suma);
            break;
        case 2:
            printf("ingrese dos valores numericos separados por coma, para realizar una operacion matematica \n");
            scanf("%f,%f", &a,&b);
            resta=fn_resta(a,b);
            printf("la resta da: %0.2f \n", resta);
            break;
        case 3:
            printf("ingrese dos valores numericos separados por coma, para realizar una operacion matematica \n");
            scanf("%f,%f", &a,&b);
            multiplicacion=fn_multiplicacion(a,b);
            printf("la multiplicacion da: %0.2f \n", multiplicacion);
            break;
        case 4:
           printf("ingrese dos valores numericos separados por coma, para realizar una operacion matematica \n");
           scanf("%f,%f", &a,&b);
           division=fn_division(a,b);
           printf("la division da; %0.2f \n", division);
           break;
        case 5:
            salir=true;
            printf("Adios");
            break;
        }    
    } while (!salir);
    

}


float fn_suma(float a, float b){
   float suma=0;
   suma=a+b;
   return suma;
}

float fn_resta(float a, float b){
   float resta=0;
   resta=a-b;
   return resta;
}

float fn_multiplicacion(float a, float b){
   float multiplicacion=0;
   multiplicacion=a*b;
   return multiplicacion;
}

float fn_division(float a, float b){
   float division=0;
   division=a/b;
   return division;
}






