#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
// Necesaria para incluir el strcpy
#include <string.h>

// Declaraci�n de las Estructuras
// Persona
struct persona {
    char nombre[12];
    char apellido[12];
    int edad;
};
// Persona2
typedef struct {
    char nombre[12];
    char apellido[12];
    int edad;
} persona2;
// Direcci�n
typedef struct {
    char calle[12];
    int numero;
} direccion;
// Empresa
typedef struct {
    char nombre[12];
    direccion ubicacion;
} empresa;

int main(){
    /// Asignar la estructura persona a la variable HIJO
    struct persona hijo;
    /// Asignar la estructura persona2 a la variable PADRE
    persona2 padre;
    /// Asignaci�n de valores a la Estrucutra PADRE
    strcpy(padre.apellido, "Ruiz");
    strcpy(padre.nombre,"Carlos");
    padre.edad=32;
    /// Asignaci�n de valores a la Estrucutra HIJO
    strcpy(hijo.apellido,"Ruiz");
    strcpy(hijo.nombre,"Esteban");
    hijo.edad=16;
    /// Muestra de los valores de las Estructuras
    printf("Padre -> Nombre: %s, Apellido : %s, Edad : %d \n", padre.nombre, padre.apellido, padre.edad);
    printf("Hijo -> Nombre: %s, Apellido : %s, Edad : %d \n", hijo.nombre, hijo.apellido, hijo.edad);
    /// Estructura Dentro de una Estructura
    empresa miEmpresa;
    strcpy(miEmpresa.nombre, "Empresa SA");
    strcpy(miEmpresa.ubicacion.calle, "Calle Falsa");
    miEmpresa.ubicacion.numero=123;
    printf("%s - Ubicacion : %s N� %d",miEmpresa.nombre,miEmpresa.ubicacion.calle,miEmpresa.ubicacion.numero);

}






