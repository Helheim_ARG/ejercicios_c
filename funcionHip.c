#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

float Pitagoras (float cA, float cB);
bool es_negativo (float a);

int main()
{
    float H, c1, c2;

    do{
    printf("ingrese los catetos de un triangulo rectangulo separados por coma \n");
    scanf("%f,%f", &c1, &c2);
    if (es_negativo (c1) || es_negativo (c2))
    {
        printf("ERROR: Numeros no validos \n");
    }
    }while (es_negativo (c1) || es_negativo (c2));
    
    
    H=Pitagoras(c1,c2);
    printf("el valor de la hipotenusa es: %0.2f", H);

    

}

float Pitagoras (float cA, float cB){
    float Hip;
    Hip=sqrt(pow(cA, 2)+ pow(cB, 2));
    return Hip;
}

bool es_negativo (float a){
    if (a<0){
     return true;
    }
    else
    {
        return false;
    }
     
}