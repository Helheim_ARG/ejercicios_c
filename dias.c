#include <stdio.h>

int main()
{
    int dias, anios, meses;
    printf("ingrese una cantidad de dias para calcular: ");
    scanf("%d", &dias);

    anios= dias/365;
    meses= (dias%365)/30;
    dias= dias - (anios*365) - (meses*30);
     
    printf("dias %d\n", dias);
    printf("meses %d\n", meses);
    printf("anios %d", anios);

    return 0;

}
