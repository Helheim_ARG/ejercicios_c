#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Profesor
{
    char Nombre[12];
    char Direccion[20];
    int Telefono; 
};

typedef struct
{
    char Nombre[12];
    char nivel[12];
    char Descripcion[100];
    int N_alumnos;
} Curso;

int main(){

    int i;

    struct Profesor profesor[3];
    Curso curso[2][2];

    strcpy(profesor[0].Nombre, "Jorge Gracia");
    strcpy(profesor[0].Direccion, "Calle San Martin");
    profesor[0].Telefono=3804;
    strcpy(profesor[1].Nombre, "Hugo Morales");
    strcpy(profesor[1].Direccion, "Calle Santa Rosa");
    profesor[1].Telefono=3827;
    strcpy(profesor[2].Nombre, "Nicolas Olivera");
    strcpy(profesor[2].Direccion, "Calle Pelagio B. Luna");
    profesor[2].Telefono=4629;
    printf("\n _Arrays_ \n\n");
    for ( i = 0; i < 3; i++)
    {
        printf("Profesor-> Nombre: %s Dirceccion: %s Telefono: %d \n", profesor[i].Nombre,profesor[i].Direccion, profesor[i].Telefono);
    }

    strcpy(curso[0][0].Nombre, "3B");
    strcpy(curso[0][0].nivel, "secundario");
    strcpy(curso[0][0].Descripcion, "Lorem ipsum dolor sit amet.");
    curso[0][0].N_alumnos=35;
    strcpy(curso[0][1].Nombre, "5A");
    strcpy(curso[0][1].nivel, "primario");
    strcpy(curso[0][1].Descripcion, "Lorem ipsum dolor sit amet.");
    curso[0][1].N_alumnos=30;
    strcpy(curso[1][0].Nombre, "2B");
    strcpy(curso[1][0].nivel, "secundario");
    strcpy(curso[1][0].Descripcion, "Lorem ipsum dolor sit amet.");
    curso[1][0].N_alumnos=30;
    strcpy(curso[1][1].Nombre, "programacion \n");
    strcpy(curso[1][1].nivel, "terciario");
    strcpy(curso[1][1].Descripcion, "Lorem ipsum dolor sit amet.");
    curso[1][1].N_alumnos=14;

    printf("\n _Matriz_ \n\n");
    for(int i=0;i<2;i++){
        for(int j=0;j<2;j++){
            printf("Curso-> Nombre: %s Nivel: %s Descripcion: %s \n", curso[i][j].Nombre, curso[i][j].nivel, curso[i][j].Descripcion);
        }
    }    
}   



