#include <stdio.h>
#include <limits.h>
int main()
{
    printf("Tipo \t\t Tamanio \t min \t max\n");
    //Enteros
    printf("Char \t\t %lu \t\t %d \t %d\n", sizeof(char), CHAR_MIN, CHAR_MAX );
    printf("short \t\t %lu \t\t %d \t %d\n", sizeof(short), SHRT_MIN, SHRT_MAX);
    printf("int \t\t %lu \t\t %d \t %d\n", sizeof(int), INT_MIN, INT_MAX);
    printf("long \t\t %lu \t\t %d \t %d\n", sizeof(long), LONG_MIN, LONG_MAX);
    printf("long long \t\t %lu \t\t %lld \t %lld\n", sizeof(long long), LONG_LONG_MIN, LONG_LONG_MAX);
    //Puntos Flotantes
    printf("float \t\t %lu \t\t %e \t %e\n", sizeof(float), __FLT_MIN__, __FLT_MAX__);
    printf("double \t\t %lu \t\t %e \t %e\n", sizeof(double), __DBL_MIN__, __DBL_MAX__);
    printf("long double \t\t %lu \t\t %e \t %e\n", sizeof(long double),__LDBL_MIN__,__LDBL_MAX__);

    

}