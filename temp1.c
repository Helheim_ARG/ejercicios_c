#include <stdio.h>
#include <math.h>

int main()
{
    float f, c;
    printf("TABLA DE FOR\n");
    printf("º C %c º\tº F %c º\n",167,167);

    for(f=0; f<=300; f=f+20)
    {
        c=(5*(f-32))/9;

        printf("º%1.0fC%cº\tº%1.0fF%cº\n",c,167,f,167);

    }
    f=0;
    printf("Tabla del while");
    while(f<=300);
    {
        c=(5*(f-32))/9;
        printf("º%1.0fC%cº\tº%1.0fF%cº\n",c,167,f,167);
        f=f+20;

    }
}