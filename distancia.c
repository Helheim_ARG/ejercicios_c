#include <stdio.h>
#include <math.h>

int main()
{
    float x1=0, y1=0, x2=0, y2=0;
    float D=0; 

    printf("ingrese las cordenadas de 2 puntos sepradas con coma en este orden: x1,y1,x2,y2\n ");
    scanf("%f,%f,%f,%f", &x1,&y1,&x2,&y2);

    D= sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));

    printf("la distancia entre estos puntos es de: %f", D);

    return 0;

}